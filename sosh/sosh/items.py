# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class IgraalItem(scrapy.Item):
    comp = scrapy.Field()
    full_rating = scrapy.Field()
    full_cachback = scrapy.Field()
    full_rapport = scrapy.Field()
    full_promotion = scrapy.Field()
    full_service = scrapy.Field()
    full_perc = scrapy.Field()

    user = scrapy.Field()
    user_reviews = scrapy.Field()
    user_rating = scrapy.Field()

    user_cachback = scrapy.Field()
    user_rapport = scrapy.Field()
    user_promotion = scrapy.Field()
    user_service = scrapy.Field()
    review_use = scrapy.Field()

    date = scrapy.Field()
    title = scrapy.Field()
    text_review = scrapy.Field()
    img = scrapy.Field()
    url = scrapy.Field()


class TrustpilotItem(scrapy.Item):
    comp = scrapy.Field()
    full_reviews = scrapy.Field()
    full_excellent = scrapy.Field()
    full_bien = scrapy.Field()
    full_moyen = scrapy.Field()
    full_bas = scrapy.Field()
    full_mauvais = scrapy.Field()
    full_trust_score = scrapy.Field()

    user = scrapy.Field()
    user_reviews = scrapy.Field()
    user_rating = scrapy.Field()
    review_utile = scrapy.Field()
    date = scrapy.Field()
    title = scrapy.Field()
    text_review = scrapy.Field()

    img = scrapy.Field()
    url = scrapy.Field()


class MonpetitforfaitItem(scrapy.Item):
    comp = scrapy.Field()
    full_reviews = scrapy.Field()
    full_rating = scrapy.Field()
    full_perc = scrapy.Field()

    user = scrapy.Field()
    user_rating = scrapy.Field()
    review_up = scrapy.Field()
    review_down = scrapy.Field()
    date = scrapy.Field()
    title = scrapy.Field()
    text_review = scrapy.Field()

    img = scrapy.Field()
    url = scrapy.Field()


class AlloforfaitItem(scrapy.Item):
    comp = scrapy.Field()
    full_reviews = scrapy.Field()
    full_rating = scrapy.Field()

    user = scrapy.Field()
    review_id = scrapy.Field()
    parent_review_id = scrapy.Field()
    date = scrapy.Field()
    review = scrapy.Field()

    url = scrapy.Field()


class QuechoisirItem(scrapy.Item):
    comp = scrapy.Field()

    user = scrapy.Field()
    user_rating = scrapy.Field()
    user_mess = scrapy.Field()
    date = scrapy.Field()
    is_auth = scrapy.Field()
    mess_id = scrapy.Field()
    post_title = scrapy.Field()
    text_mess = scrapy.Field()

    url = scrapy.Field()


class SoshforumItem(scrapy.Item):
    category = scrapy.Field()
    post_id = scrapy.Field()
    post_title = scrapy.Field()
    is_topic = scrapy.Field()

    user = scrapy.Field()

    mess_rating = scrapy.Field()
    mess_id = scrapy.Field()
    date = scrapy.Field()
    mess_text = scrapy.Field()
    text_mess = scrapy.Field()
    url = scrapy.Field()
    img1 = scrapy.Field()
    img2 = scrapy.Field()
    img3 = scrapy.Field()
    img4 = scrapy.Field()
    img5 = scrapy.Field()
    img6 = scrapy.Field()
    img7 = scrapy.Field()
    img8 = scrapy.Field()
    img9 = scrapy.Field()
    img10 = scrapy.Field()
