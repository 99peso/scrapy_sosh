# -*- coding: utf-8 -*-
import scrapy
import datetime
from sosh.items import SoshforumItem

SET = set()
USERS = set()


class SoshforumSpider(scrapy.Spider):
    name = 'soshforum'
    allowed_domains = ['communaute.sosh.fr']
    start_urls = ['https://communaute.sosh.fr/t5/Mes-premiers-pas-Ma-pr%C3%A9sentation/bd-p/premiers-pas-presentation',
                  'https://communaute.sosh.fr/t5/Trucs-Astuces-des-Sosheurs/idb-p/trucs-et-astuces',
                  'https://communaute.sosh.fr/t5/Parrainage/bd-p/parrainage',
                  'https://communaute.sosh.fr/t5/Au-caf%C3%A9-des-Sosheurs/bd-p/cafe-des-sosheurs',

                  'https://communaute.sosh.fr/t5/Changer-mon-offre-mes-options/bd-p/changer-offre-options-demenager',
                  'https://communaute.sosh.fr/t5/Ma-facture-mon-paiement-Mon/bd-p/facture-paiement-suivi-conso',
                  'https://communaute.sosh.fr/t5/Ma-ligne-Internet-Livebox-email/bd-p/deja-client-ligne-fixe-livebox',
                  'https://communaute.sosh.fr/t5/Ma-ligne-Mobile-SIM-PUK/bd-p/deja-client-ligne-mobile',
                  'https://communaute.sosh.fr/t5/Ma-t%C3%A9l%C3%A9vision-par-internet/bd-p/television-tv',

                  'https://communaute.sosh.fr/t5/Livraison-ma-carte-SIM-mes/bd-p/livraison',
                  'https://communaute.sosh.fr/t5/Ma-ligne-Mobile-activation/bd-p/nouveau-client-ligne-mobile',
                  'https://communaute.sosh.fr/t5/Ma-ligne-Internet-configuration/bd-p/nouveau-client-ligne-fixe-livebox',
                  'https://communaute.sosh.fr/t5/Portabilit%C3%A9-de-mon-num%C3%A9ro-mobile/bd-p/portabilite-numero',
                  'https://communaute.sosh.fr/t5/Mes-offres-de-remboursement-Ma/bd-p/odr-offre-remboursement-promo-filleul',

                  'https://communaute.sosh.fr/t5/Les-forfaits-Sosh-Mobile-Les/bd-p/forfaits-sosh-mobile-options',
                  'https://communaute.sosh.fr/t5/La-Bo%C3%AEte-Sosh-internet-%C3%A0-la/bd-p/forfaits-livebox-options',

                  'https://communaute.sosh.fr/t5/Sosh-grandit-gr%C3%A2ce-%C3%A0-vous/ct-p/coconstruction',
                  'https://communaute.sosh.fr/t5/Avant-de-souscrire/ct-p/avant-souscrire',
                  'https://communaute.sosh.fr/t5/Nouveau-Client-Prendre-en-main/ct-p/nouveau-client',
                  'https://communaute.sosh.fr/t5/D%C3%A9j%C3%A0-client-G%C3%A9rer-mon-offre/ct-p/deja-client',
                  'https://communaute.sosh.fr/t5/Echanger-entre-Sosheurs/ct-p/entre-sosheurs'
                  ]

    custom_settings = {'FEED_FORMAT': 'csv',
                       'FEED_URI': '{}.csv'.format(name),
                       'FEED_EXPORT_FIELDS': ["category", "post_id", "post_title", "is_topic", "user", "mess_rating",
                                              "mess_id", "date", "mess_text", "url",
                                              "img1", "img2", "img3", "img4", "img5", "img6", "img7", "img8", "img9", "img10"]
                        }

    def parse(self, response):
        meta = {'user': response.meta.get('user')}
        new_urls = response.xpath("//h2/span/a/@href").extract()
        pagin_url = response.xpath("//li[contains(@class, lia-paging-page-link)]/a/@href").extract()
        unic_urls = set(pagin_url)
        for url in unic_urls:
            if 'page' in url:
                yield response.follow(url, callback=self.parse, meta=meta)

        for new in new_urls:
            yield response.follow(new, callback=self.parse_item, meta=meta)

    def parse_item(self, response):
        global SET, USERS
        host = 'https://communaute.sosh.fr'
        url = response.url
        post_id = url.split('/page/')[0].split('/')[-1]
        post_title = response.xpath("//h1/text()").extract()[0]
        category = response.xpath("//span[contains(@class, 'category')]/a/text()").extract()[0]

        cards = response.xpath("//div[contains(@class, 'lia-quilt-forum-message')]")
        for card in cards:
            item = SoshforumItem()
            try:
                item['user'] = card.xpath(".//a[contains(@class, 'lia-user-name-link')]/span/text()").extract()[0]
            except:
                continue

            if item['user'] != response.meta.get('user') and response.meta.get('user'):
                continue

            item['url'] = url
            item['category'] = category
            item['post_id'] = post_id
            item['post_title'] = post_title

            item['is_topic'] = 1 if 'topic' in card.xpath(".//../@class").extract()[0] else 0

            item['mess_rating'] = card.xpath(".//span[contains(@itemprop, 'upvoteCount')]/text()").extract()[0].strip()
            item['mess_id'] = card.xpath(".//a[contains(@class, 'lia-component-forums-action-highlight-message')]"
                                         "/@href").extract()[0].split('#M')[-1]
            user_id = card.xpath(".//a[contains(@class, 'lia-user-name-link')]/@href").extract()[0].split('/')[-1]

            if user_id not in USERS:
                USERS.add(user_id)
                tmp_url = 'https://communaute.sosh.fr/t5/forums/recentpostspage/post-type/message/user-id/{}'
                user_url = tmp_url.format(user_id)
                yield response.follow(user_url, callback=self.parse, meta={'user': item['user']})

            predate = card.xpath("//span[contains(@class, 'local-date')]/text()").extract()[0].split(' ')[-1][1:]
            item['date'] = datetime.datetime.strptime(predate, '%d-%m-%Y').strftime('%Y-%m-%d')

            item['mess_text'] = '\n'.join(card.xpath(".//div[contains(@class, 'lia-message-body-content')]"
                                                     "/p/text()").extract())
            imgs = card.xpath(".//img[contains(@class, 'lia-media-image')]/@src").extract()

            for i, tmp in enumerate(imgs):
                img = tmp.split('/image-size')[0]
                if 'https' not in img:
                    img = host + img
                if i < 10:
                    item['img{}'.format(i+1)] = img

            if str((item['mess_id'], item['post_id'], item['user'])) not in SET:
                SET.add(str((item['mess_id'], item['post_title'], item['user'])))
                yield item

