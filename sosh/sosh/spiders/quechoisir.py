# -*- coding: utf-8 -*-
import scrapy
from sosh.items import QuechoisirItem


class QuechoisirSpider(scrapy.Spider):
    name = 'quechoisir'
    allowed_domains = ['forum.quechoisir.org']
    start_urls = ['https://forum.quechoisir.org/recherche.php?keywords=sosh',
                  'https://forum.quechoisir.org/recherche.php?keywords=redbysfr',
                  'https://forum.quechoisir.org/recherche.php?keywords=b-you',
                  'https://forum.quechoisir.org/recherche.php?keywords=free+mobile'
                  ]
    custom_settings = {'FEED_FORMAT': 'csv',
                       'FEED_URI': '{}.csv'.format(name),
                       'FEED_EXPORT_FIELDS': ['comp', 'user', 'user_rating', 'user_mess', 'date', 'is_auth', 'mess_id',
                                              'post_title', 'text_mess', 'url']
                      }

    def parse(self, response):
        comp = ''
        host = 'https://forum.quechoisir.org'
        url = response.url
        if 'sosh' in url:
            comp = 'Sosh'
        elif 'redbysfr' in url:
            comp = 'RED by SFR'
        elif 'b-you' in url:
            comp = 'B & YOU'
        elif 'free+mobile' in url:
            comp = 'Freemobile'

        nexturls = response.xpath("//div[contains(@class, 'ufc-search-postbody')]/div/h2/a/@href").extract()
        for tmp in nexturls:
            nxt = host + tmp.split('#')[0]
            yield response.follow(nxt, callback=self.parse_items, meta={'tmp': comp})

    def parse_items(self, response):
        moreurls = response.xpath("//a[contains(@role, 'button')]/@href").extract()
        comp = response.meta.get('tmp')

        for newurl in moreurls:
            yield response.follow(newurl, callback=self.parse_items, meta={'tmp': comp})

        cards = response.xpath("//div[contains(@class, 'has-profile')]/div[contains(@class, 'inner')]")
        for card in cards:
            item = QuechoisirItem()
            item['comp'] = comp
            item['user'] = card.xpath(".//a[contains(@class, 'username-coloured')]/text()").extract()[0]
            date = card.xpath(".//p[contains(@class, 'author')]/text()").extract()[0]
            item['date'] = self.clean_date(date)
            item['url'] = response.url
            item['user_rating'] = card.xpath(".//dd[contains(@class, 'profile-rank')]/text()").extract()[0]
            item['user_rating'] = item['user_rating'].count('*') if item['user_rating'].count('*') else 10
            item['user_mess'] = card.xpath(".//dd[contains(@class, 'profile-posts')]/a/text()").extract()[0]
            pre_title = card.xpath(".//h2/a/text()").extract()[0]
            item['mess_id'] = card.xpath(".//h2/a/@href").extract()[0].split('p')[-1]
            item['is_auth'] = 0 if pre_title.count('Re:') else 1
            item['post_title'] = pre_title.replace('Re:', '').strip()
            item['text_mess'] = '\n'.join(card.xpath(".//div[contains(@class, 'content')]/text()").extract())

            yield item

    def clean_date(self, date):
        day, month, year = date.split(',')[0].split()
        mdict = {'janv.': '01',
                 'févr.': '02',
                 'mars': '03',
                 'avr.': '04',
                 'mai': '05',
                 'juin': '06',
                 'juil.': '07',
                 'août': '08',
                 'sept.': '09',
                 'oct.': '10',
                 'nov.': '11',
                 'déc.': '12'
                 }
        return '-'.join((year, mdict.get(month.encode('utf8')), day))
