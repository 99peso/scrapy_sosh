# -*- coding: utf-8 -*-
import scrapy
from sosh.items import AlloforfaitItem


class AlloforfaitSpider(scrapy.Spider):
    name = 'alloforfait'
    allowed_domains = ['alloforfite.fr']
    start_urls = ['https://alloforfait.fr/mobile/red',
                  'https://alloforfait.fr/mobile/sosh',
                  'https://alloforfait.fr/mobile/b-and-you',
                  'https://alloforfait.fr/mobile/free-mobile']

    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': '{}.csv'.format(name),
        'FEED_EXPORT_FIELDS': ['comp', 'full_reviews', 'full_rating', 'user', 'review_id',
                               'parent_review_id', 'date', 'review', 'url']
        }

    def gen_comp(self, url):
        comp = ''
        if 'sosh' in url:
            comp = 'Sosh'
        elif 'red' in url:
            comp = 'RED by SFR'
        elif 'b-and-you' in url:
            comp = 'B & YOU'
        elif 'free-mobile' in url:
            comp = 'Freemobile'
        return comp

    def parse(self, response):

        comp = self.gen_comp(response.url)
        _,  tmp = response.xpath("//div[contains(@class, 'post-ratings')]/strong/text()").extract()
        full_rating = tmp.replace(',', '.')
        tmp = response.xpath("//div[contains(@id, 'comments')]/div/div[contains(@class, 'enune')]/text()").extract()[0]
        full_reviews = tmp.split(' ')[-1].strip('()')

        tmp = response.xpath("//div[contains(@id, 'comments')]")
        tmp = tmp.xpath(".//*/div[contains(@class, 'comm')] | .//*/*/div[contains(@class, 'comm')]")

        for i, card in enumerate(tmp):
            item = AlloforfaitItem()
            item['comp'] = comp
            item['full_reviews'] = full_reviews
            item['full_rating'] = full_rating
            parent_class = card.xpath('.//../@class').extract()[0]
            if parent_class == 'children':
                item['parent_review_id'] = tmp[i-1].xpath("./@id").extract()[0].split('-')[-1]
            else:
                item['parent_review_id'] = ''
            item['review_id'] = card.xpath("./@id").extract()[0].split('-')[-1]
            predate = card.xpath(".//em/text()").extract()[0].split(' ')[:3]
            item['date'] = self.clean_date(predate)
            item['review'] = '\n'.join(card.xpath(".//p/text() | .//*/p/text()").extract())
            item['url'] = response.url
            yield item

    def clean_date(self, date):
        day, month, year = date
        mdict = {'janv.': '01',
                 'févr.': '02',
                 'mars': '03',
                 'avril': '04',
                 'mai': '05',
                 'juin': '06',
                 'juil.': '07',
                 'août': '08',
                 'septembre': '09',
                 'octobre': '10',
                 'novembre': '11',
                 'décembre': '12'
                 }
        return '-'.join((year, mdict.get(month.encode('utf8')), day))
