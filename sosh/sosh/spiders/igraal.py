# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from sosh.items import IgraalItem
import itertools


class IgraalSpider(scrapy.Spider):
    name = 'igraal'
    allowed_domains = ['fr.igraal.com/avis']
    pre_urls = ['https://fr.igraal.com/avis/Sosh',
                'https://fr.igraal.com/avis/sfr-red',
                'https://fr.igraal.com/avis/BetYou']

    start_urls = list(map(lambda x: x[0] + '/' + str(x[1]),
                          itertools.product(pre_urls, range(1, 100))))

    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': '{}.csv'.format(name),
        'FEED_EXPORT_FIELDS': ['comp', 'full_rating', 'full_cachback', 'full_rapport', 'full_promotion', 'full_service',
                               'full_perc', 'user', 'user_reviews', 'user_rating',
                               'user_cachback', 'user_rapport', 'user_promotion', 'user_service',
                               'date', 'title', 'text_review', 'review_use', 'url', 'img']
    }

    def get_rate(self, raw):
        rate = (raw.count('star-active')-1) / 2.0
        return rate

    def parse(self, response):
        pre = response.xpath("//strong[contains(@class, 'review__note')]/text()").extract()
        full_rating = str(pre[0].split(' / ')[0].replace(',', '.'))

        pre = response.xpath("//article[contains(@class, 'review__item--first')]"
                             "/div/div[contains(@class, 'review__detail-rating-wrap')]").extract()
        try:
            full_cachback, full_rapport, full_promotion, full_service = [str(self.get_rate(row)) for row in pre]
        except:
            full_cachback, full_rapport, full_promotion, full_service = '', '', '', ''
        full_perc = str(response.xpath("//strong[contains(@class, 'review__fb-percent')]/text()").extract()[0])

        precards = response.xpath("//article[contains(@class, 'review__item')]")
        url = response.url

        if 'Sosh' in url:
            comp = 'Sosh'
        elif 'sfr-red' in url:
            comp = 'SFR Red'
        elif 'BetYou' in url:
            comp = 'B&You'
        else:
            comp = ''

        for pre in precards[1:]:
            item = IgraalItem()
            item['full_rating'] = full_rating
            item['full_cachback'] = full_cachback
            item['full_rapport'] = full_rapport
            item['full_promotion'] = full_promotion
            item['full_service'] = full_service
            item['full_perc'] = full_perc
            item['url'] = url
            item['comp'] = comp

            item['user'] = pre.xpath(".//strong[contains(@class, 'review__left-txt')]/text()").extract()[0]
            item['user_reviews'] = \
                pre.xpath(".//span[contains(@class, 'review__total')]/text()").extract()[0].split(' ')[0]
            item['user_rating'] = \
                pre.xpath(".//strong[contains(@class, 'review__note')]/text()").extract()[0].split(' / ')[0].replace(
                    ',', '.')
            tmp = pre.xpath(".//div[contains(@class, 'review__detail-rating-wrap')]").extract()
            try:
                item['user_cachback'], item['user_rapport'], item['user_promotion'], item['user_service'] = [
                    str(self.get_rate(row)) for row in tmp]
            except:
                item['user_cachback'], item['user_rapport'], item['user_promotion'], item[
                    'user_service'] = '', '', '', ''

            item['date'] = \
                pre.xpath(".//div[contains(@class, 'review__header-date')]/text()").extract()[0].strip().split(' ')[-1]
            try:
                item['title'] = pre.xpath(".//strong[contains(@class, 'review__title')]/text()").extract()[0]
            except:
                item['title'] = ''

            try:
                item['text_review'] = pre.xpath(".//p[contains(@class, 'review__desc')]/text()").extract()[0]
            except:
                item['text_review'] = ''
            item['review_use'] = 'NO'

            yield item



