# -*- coding: utf-8 -*-
import scrapy
from sosh.items import MonpetitforfaitItem
import urllib
import json
from scrapy.selector import Selector
import re


TEMPLSET = set()


class MonpetitforfaitSpider(scrapy.Spider):
    name = 'monpetitforfait'
    allowed_domains = ['www.monpetitforfait.com']
    start_urls = ['https://www.monpetitforfait.com/sosh/avis-sosh',
                  'https://www.monpetitforfait.com/red-by-sfr/avis-red-by-sfr',
                  'https://www.monpetitforfait.com/b-and-you/avis-b-and-you',
                  'https://www.monpetitforfait.com/free-mobile/avis-free-mobile']

    custom_settings = {'FEED_FORMAT': 'csv',
                       'FEED_URI': '{}.csv'.format(name),
                       'FEED_EXPORT_FIELDS': ['comp', 'full_reviews', 'full_rating', 'full_perc', 'user',
                                              'user_rating', 'review_up', 'review_down', 'date', 'title', 'text_review']
                        }

    def parse(self, response):

        HEADERS = {'Host': 'www.monpetitforfait.com',
                   'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0',
                   'Accept': 'application/json, text/javascript, */*; q=0.01',
                   'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                   'Accept-Encoding': 'gzip, deflate, br',
                   'Referer': '',
                   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'X-Requested-With': 'XMLHttpRequest',
                   'Connection': 'keep-alive',
                   'Cookie': 'wp_mpf_first_time=true; wp_mpf_desktop_offer=true',
                   'Pragma': 'no-cache',
                   'Cache-Control': 'no-cache'}

        url = 'https://www.monpetitforfait.com/wp-admin/admin-ajax.php'
        lenpages = int(response.xpath("//a[contains(@class, 'reviews-counte')]/text()").extract()[0].split(' ')[0][1:])
        pages = 30
        ids = re.findall('data-nonce="[^"]+', response.text)[0].split('"')[-1]
        full_rating = response.xpath("//div[contains(@class, 'reviews-rating')]/span/text()").extract()[0]
        full_perc = response.xpath("//div[contains(@class, 'details-recommended')]"
                                   "/span[contains(@class, 'text')]/text()").extract()[0].split('&percnt')[0] + '%'

        data = {'action': 'ac_load_reviews',
                'order': 'useful',
                'pagination': '0',
                'nonce': ids}

        HEADERS['Referer'] = response.url

        next = (lenpages, full_rating, full_perc, response.url)
        for i in range(1, pages):
            for order in ['useful', 'best', 'recent', 'worst']:
                data['pagination'] = str(i)
                data['order'] = order
                yield scrapy.Request(url, callback=self.parse_items, headers=HEADERS, method='POST',
                                     body=urllib.urlencode(data), meta={'tmp': next}, dont_filter=True)

    def parse_items(self, response):
        global TEMPLSET
        raw = Selector(text=json.loads(response.text).get('html'))

        cards = raw.xpath("//div[contains(@class, 'review-card')]")

        lenpages, full_rating, full_perc, url = response.meta.get('tmp')
        for card in cards:
            item = MonpetitforfaitItem()
            item['full_rating'] = full_rating
            item['full_perc'] = full_perc
            item['full_reviews'] = lenpages
            comp = ''
            if 'sosh' in url:
                comp = 'Sosh'
            elif 'red-by-sfr' in url:
                comp = 'RED by SFR'
            elif 'b-and-you' in url:
                comp = 'B & YOU'
            elif 'free-mobile' in url:
                comp = 'Freemobile'
            item['comp'] = comp
            item['user'] = card.xpath(".//div[contains(@class, 'comment-nickname')]/text()").extract()[0]
            item['user_rating'] = re.findall('ratingValue[^,]+', card.xpath(".//script/text()").extract()[-1])[0].split('"')[-2]
            tmp = card.xpath(".//em/text()").extract()
            item['review_up'], item['review_down'] = tmp[0].strip('()'), tmp[1].strip('()')
            item['date'] = card.xpath(".//div[contains(@class, 'comment-date')]/text()").extract()[0].split(' ')[-1]
            item['title'] = card.xpath(".//span[contains(@class, 'text')]/text()").extract()[0]
            item['text_review'] = card.xpath(".//div[contains(@class, 'comment-content')]/text()").extract()[0]

            if str((item['user'], item['date'], item['comp'])) not in TEMPLSET:
                TEMPLSET.add(str((item['user'], item['date'], item['comp'])))
                yield item
