# -*- coding: utf-8 -*-
import scrapy
from sosh.items import TrustpilotItem
import re


class TrustpilotSpider(scrapy.Spider):
    name = 'trustpilot'
    allowed_domains = ['fr.trustpilot.com']

    start_urls = ['https://fr.trustpilot.com/review/sosh.fr',
                  'https://fr.trustpilot.com/review/red-by-sfr.fr',
                  'https://fr.trustpilot.com/review/b-and-you.fr',
                  'https://fr.trustpilot.com/review/freemobile.fr']

    custom_settings = {'FEED_FORMAT': 'csv',
                       'FEED_URI': '{}.csv'.format(name),
                       'FEED_EXPORT_FIELDS': ['comp', 'full_reviews', 'full_excellent', 'full_bien', 'full_moyen',
                                              'full_bas', 'full_mauvais', 'full_trust_score', 'user', 'user_reviews',
                                              'user_rating', 'review_utile', 'date', 'title', 'text_review', 'url']
                        }

    def parse(self, response):
        len_rewiews = int(response.xpath("//span[contains(@class, 'headline__review-count')]/text()").extract()[0])
        for i in range(1, int(len_rewiews / 20) + 2):
            yield response.follow(response.url + '?page=' + str(i), self.parse_item)

    def parse_additional(self, response):
        host = 'https://fr.trustpilot.com'
        item = TrustpilotItem()
        companies = response.xpath("//div[contains(@class, 'company-name')]/a/text()").extract()
        cards = response.xpath("//div[contains(@class, 'review-card')]")
        for i, card in enumerate(cards):
            item['comp'] = companies[i]
            item['user'] = card.xpath(".//div[contains(@class, 'consumer-information__name')]/text()").extract()[
                0].strip()
            item['user_reviews'] = card.xpath(".//div[contains(@class, 'consumer-information__review-count')]"
                                              "/span/text()").extract()[0].split(' ')[0]
            item['user_rating'] = \
            card.xpath(".//div[contains(@class, 'star-rating')]/img/@alt").extract()[0].split(' ')[0]

            pre_review_utile = int(
                card.xpath(".//brand-find-useful-button/@*[name()=':initial-find-useful-count']").extract()[0])

            item['review_utile'] = 'Yes' if pre_review_utile else 'No'

            item['date'] = card.xpath(".//script[contains(@data-initial-state, 'review-dates')]").extract()[0].split(
                'publishedDate":"')[-1].split('T')[0]
            item['title'] = card.xpath('.//h2/a/text()').extract()[0].strip()
            item['text_review'] = card.xpath(".//p[contains(@class, 'review-content__text')]/text()").extract()[
                0].strip()

            item['url'] = host + card.xpath(".//h2[contains(@class, 'review-content__title')]/a/@href").extract()[0]

            yield item

    def parse_item(self, response):
        host = 'https://fr.trustpilot.com'
        comp = response.xpath("//span[contains(@class, 'multi-size-header__big')]/text()").extract()[0]
        full_reviews = response.xpath("//span[contains(@class, 'headline__review-count')]/text()").extract()[0]

        full_excellent = re.findall('5 .toile[^\]]+%', response.text)[0].split('"')[-1]
        full_bien = re.findall('4 .toile[^\]]+%', response.text)[0].split('"')[-1]
        full_moyen = re.findall('3 .toile[^\]]+%', response.text)[0].split('"')[-1]
        full_bas = re.findall('2 .toile[^\]]+%', response.text)[0].split('"')[-1]
        full_mauvais = re.findall('1 .toile[^\]]+%', response.text)[0].split('"')[-1]

        full_trust_score = re.findall('trustScore":[^,]+', response.text)[0].split(':')[-1]

        cards = response.xpath("//div[contains(@class, 'review-card')]")
        for card in cards:
            item = TrustpilotItem()

            item['comp'] = comp
            item['full_reviews'] = full_reviews
            item['full_excellent'] = full_excellent
            item['full_bien'] = full_bien
            item['full_moyen'] = full_moyen
            item['full_bas'] = full_bas
            item['full_moyen'] = full_moyen
            item['full_mauvais'] = full_mauvais
            item['full_trust_score'] = full_trust_score

            item['user'] = card.xpath(".//div[contains(@class, 'consumer-information__name')]/text()").extract()[0].strip()
            item['user_reviews'] = card.xpath(".//div[contains(@class, 'consumer-information__review-count')]"
                                      "/span/text()").extract()[0].split(' ')[0]
            item['user_rating'] = card.xpath(".//div[contains(@class, 'star-rating')]/img/@alt").extract()[0].split(' ')[0]
            pre_review_utile = int(card.xpath(".//brand-find-useful-button/@*[name()=':initial-find-useful-count']").extract()[0])

            item['review_utile'] = 'Yes' if pre_review_utile else 'No'

            item['date'] = card.xpath(".//script[contains(@data-initial-state, 'review-dates')]").extract()[0].split('publishedDate":"')[-1].split('T')[0]
            item['title'] = card.xpath('.//h2/a/text()').extract()[0].strip()
            item['text_review'] = card.xpath(".//p[contains(@class, 'review-content__text')]/text()").extract()[0].strip()
            user_url = host + card.xpath(".//a[contains(@class, 'consumer-information')]/@href").extract()[0]
            yield response.follow(user_url, self.parse_additional)

            item['url'] = host + card.xpath(".//h2[contains(@class, 'review-content__title')]/a/@href").extract()[0]

            yield item
